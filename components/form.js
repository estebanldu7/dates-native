import React, {useState} from 'react';
import {
    Text,
    StyleSheet,
    View,
    TouchableHighlight,
    TextInput,
    Button,
    Alert,
    ScrollView,
} from 'react-native';
import DateTimePickerModal from 'react-native-modal-datetime-picker';
import shortid from 'shortid';

const Form = ({dates, setMeetings, setShowForm, storeDates}) => {

    const [patient, setPatient] = useState('');
    const [owner, setOwner] = useState('');
    const [phone, setPhone] = useState('');
    const [symptoms, setSymptoms] = useState('');
    const [date, setDate] = useState();
    const [hour, setHour] = useState();
    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirm = date => {
        const options = {year: 'numeric', month: 'long', day: '2-digit'};
        setDate(date.toLocaleDateString('es-ES', options));
        hideDatePicker();
    };

    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };

    const handleHourConfirm = hour => {
        const options = {hour: 'numeric', minute: '2-digit'};
        setHour(hour.toLocaleString('es-ES', options));
        hideTimePicker();
    };

    const createNewDate = () => {
        if (patient.trim() === '' ||
            owner.trim() === '' ||
            phone.trim() === '' ||
            date.trim() === '' ||
            hour.trim() === '' ||
            symptoms.trim() === '') {
            showAlert();
            return;
        }

        const meeting = {patient, owner, phone, date, hour, symptoms};
        meeting.id = shortid.generate();
        const newDates = [...dates, meeting];
        setMeetings(newDates);
        storeDates(JSON.stringify(newDates));
        setShowForm(false);
    };

    const showAlert = () => {
        Alert.alert(
            'Error',
            'Todos los campos son obligatorios',
            [{
                text: 'OK',
            }],
        );
    };

    return (
        <>
            <ScrollView style={styles.form}>
                <View>
                    <Text style={styles.labels}>Paciente: </Text>
                    <TextInput
                        onChangeText={text => setPatient(text)}
                        style={styles.input}/>
                </View>
                <View>
                    <Text style={styles.labels}>Dueño: </Text>
                    <TextInput
                        onChangeText={text => setOwner(text)}
                        style={styles.input}/>
                </View>
                <View>
                    <Text style={styles.labels}>Teléfono contacto: </Text>
                    <TextInput
                        onChangeText={text => setPhone(text)}
                        style={styles.input}
                        keyboardType="numeric"
                    />
                </View>
                <View>
                    <Text>Fecha: </Text>
                    <Button title="Seleccionar fechas" onPress={showDatePicker}/>
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirm}
                        onCancel={hideDatePicker}
                        locale="es_ES"
                        headerTextIOS="Elige una fecha"
                        cancelTextIOS="Canelar"
                        confirmTextIOS="Confirmar"
                    />
                    <Text>{date}</Text>
                </View>
                <View>
                    <Text>Hora: </Text>
                    <Button title="Seleccionar Hora" onPress={showTimePicker}/>
                    <DateTimePickerModal
                        isVisible={isTimePickerVisible}
                        mode="time"
                        onConfirm={handleHourConfirm}
                        onCancel={hideTimePicker}
                        locale="es_ES"
                        headerTextIOS="Elige una hora"
                        cancelTextIOS="Canelar"
                        confirmTextIOS="Confirmar"
                        is24Hour
                    />
                    <Text>{hour}</Text>
                </View>
                <View>
                    <Text style={styles.labels}>Sintomas: </Text>
                    <TextInput
                        multiline
                        onChangeText={text => setSymptoms(text)}
                        style={styles.input}
                    />
                </View>

                <TouchableHighlight
                    style={styles.btnSave}
                    onPress={() => createNewDate()}>
                    <Text style={styles.textSave}>Guardar Cita </Text>
                </TouchableHighlight>
            </ScrollView>
        </>

    );
};

const styles = StyleSheet.create({
    form: {
        backgroundColor: '#fff',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    labels: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 20,
    },
    input: {
        marginTop: 10,
        height: 50,
        borderColor: '#e1e1e1',
        borderWidth: 1,
        borderStyle: 'solid',
    },
    btnSave: {
        padding: 10,
        backgroundColor: '#5d091a',
        marginVertical: 10,
    },
    textSave: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export default Form;
