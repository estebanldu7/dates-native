/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {useState, useEffect} from 'react';
import {
    Text,
    StyleSheet,
    View,
    FlatList,
    TouchableHighlight,
    TouchableWithoutFeedback,
    Platform,
    Keyboard,
} from 'react-native';
import AsyncStorage from '@react-native-community/async-storage';
import Meeting from './components/meeting';
import Form from './components/form';

const App = () => {
    const [showForm, setShowForm] = useState(false);
    const [meetings, setMeetings] = useState('');

    useEffect(() => {
        const getDates = async () => {
            try {
                const datesStorage = await AsyncStorage.getItem('dates');

                if (datesStorage !== null) {
                    setMeetings(JSON.parse(datesStorage));
                }
            } catch (e) {
                // error reading value
            }
        };

        getDates();
    }, []);

    //delete patients from state
    const deletePatient = id => {
        const filterDates = meetings.filter(meeting => meeting.id !== id);
        setMeetings(filterDates);
        storeDates(JSON.stringify(filterDates));
    };

    const showFormFn = () => {
        setShowForm(!showForm);
    };

    const closeKeyboard = () => {
        Keyboard.dismiss();
    };

    const storeDates = async (JSONDates) => {
        try {
            await AsyncStorage.setItem('dates', JSONDates);
        } catch (e) {
            // saving error
        }
    }

    return (
        <TouchableWithoutFeedback onPress={() => closeKeyboard()}>
            <View style={styles.content}>
                <Text style={styles.title}>Administrador de Citas</Text>
                <TouchableHighlight
                    style={styles.btnShowForm}
                    onPress={() => showFormFn()}>
                    <Text style={styles.textShowForm}>{showForm ? 'Ver Citas' : 'Crear Nueva Citas'}</Text>
                </TouchableHighlight>

                <View style={styles.container}>
                    {showForm ? (
                        <Form
                            dates={meetings}
                            setMeetings={setMeetings}
                            setShowForm={setShowForm}
                            storeDates={storeDates}
                        />
                    ) : (
                        <>
                            <Text
                                style={styles.title}>{meetings.length > 0 ? 'Administra tus citas' : 'No existen citas'}
                            </Text>
                            <FlatList
                                style={styles.list}
                                data={meetings}
                                renderItem={({item}) => (
                                    <Meeting
                                        item={item}
                                        deletePatient={deletePatient}
                                    />
                                )}
                                keyExtractor={meeting => meeting.id}
                            />
                        </>
                    )}
                </View>
            </View>
        </TouchableWithoutFeedback>
    );
};

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: '2.5%',
    },
    content: {
        flex: 1,
        backgroundColor: '#AA076B',
    },
    title: {
        marginTop: Platform.OS === 'ios' ? 40 : 20,
        fontSize: 24,
        fontWeight: 'bold',
        textAlign: 'center',
        color: '#fff',
        marginBottom: 20,
    },
    list: {
        flex: 1,
    },
    btnShowForm: {
        padding: 10,
        backgroundColor: '#5d091a',
        marginVertical: 10,
    },
    textShowForm: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
    },
});

export default App;
